import sys

## Rooms
from rooms.cafeteria import Cafeteria
from rooms.cheesecakeFactory import CheesecakeFactory
from rooms.corridor import Corridor
from rooms.elevator import Elevator
from rooms.hall import Hall
from rooms.laundryRoom import LaundryRoom
from rooms.letterbox import Letterbox
from rooms.lRoom import LRoom
from rooms.lLab import LLab
from rooms.lobby import Lobby
from rooms.lsBathroom import Bathroom
from rooms.lsHallway import Hallway
from rooms.lsKitchen import Kitchen
from rooms.lsLounge import Lounge
from rooms.pLounge import PLounge
from rooms.pBathroom import PBathroom
from rooms.pRoom import PRoom
from rooms.portal import Portal
from rooms.roof import Roof
from rooms.sRoom import SRoom
from rooms.sOffice import SOffice
from rooms.stairwell import Stairwell
from rooms.toothbrush import Toothbrush
from rooms.laptop import Laptop
from rooms.textgame import TextGame
from rooms.troll import Troll


## Objects
from objects.backpack import Backpack
from objects.passphrase import Passphrase

class Escape(object):

    def __init__(self):

        #print(sys.version)
        self.createRooms()
        self.backpack = Backpack()
        self.passphrase = Passphrase()

        self.count = 0
        
    def createRooms(self):
        self.rooms = {
            "Cafeteria": Cafeteria(),
            "Cheesecake Factory": CheesecakeFactory(),
            "Corridor": Corridor(),
            "Elevator": Elevator(),
            "Hall": Hall(),
            "Laundry Room": LaundryRoom(),
            "Letterbox": Letterbox(),
            "Leonard's Room": LRoom(),
            "Leonard's Lab": LLab(),
            "Lobby": Lobby(),
            "Bathroom": Bathroom(),
            "Hallway": Hallway(),
            "Kitchen": Kitchen(),
            "Lounge": Lounge(),
            "Penny's Lounge": PLounge(),
            "Penny's Bathroom": PBathroom(),
            "Penny's Room": PRoom(),
            "Portal": Portal(),
            "Roof": Roof(),
            "Sheldon's Room": SRoom(),
            "Sheldon's Office": SOffice(),
            "Stairwell": Stairwell(),
            "Toothbrush": Toothbrush(),
            "Laptop": Laptop(),
            "textgame": TextGame(),
            "Troll": Troll()
        }

    def start(self):
        print("")
        print("")
        print("You've fallen into the set of The Big Bang Theory.")
        print("Navigate your way through the set to find your way out.")
        print("\n\n")
        print("Type 'Backpack' at any time to view what items you have collected.")
        print("")
        self.enterRoom("Lounge")


    def openDoor(self, door, room):
        if door.requiredItem == None:   
            self.enterRoom(door.leadsTo)
        else:
            item = self.backpack.getItem(door.requiredItem)

            if item != None:
                print("Used " + item.name)
                self.enterRoom(door.leadsTo)
            else:
                print("It looks like you need a " + door.requiredItem)
                self.askWhatToDo(room)


    def enterRoom(self, roomName):
        room = self.rooms[roomName]
        print(room.name)
        print(room.description)
        if room.name == "Troll":
            if self.count == 2:
                print("You defeated the troll!")
                self.passphrase.updateStateToTrue()
                self.passphrase.displayPassphrase()
                self.enterRoom("Lounge")
            else:
                self.count += 1
                self.askWhatToDo(self.rooms["Troll"])
        elif room.name == "Cheesecake Factory":
            if self.passphrase in self.backpack:
                self.end()
        else:
            self.askWhatToDo(room)

    def askWhatToDo(self, room):    
        # print out the options
        print("")
        print("Options:")

        # list the door options
        for door in room.doors:
            print ("[" + door.button + "]")

        for item in room.items:
            print("[" + item.button + "]")

        # ask the user to make a choice
        print("")
        buttonPressed = input(">>> ")
        print("")
        if buttonPressed == "Backpack":
            if len(self.backpack.items) == 0:
                print("You currently have no items in your backpack")
            else:
                print("Items in your backpack:")
                for item in self.backpack.items:
                    print("    " + item.name)
            print("")
            self.askWhatToDo(room)
        else:
            # try to find a door associated with that button
            chosenDoor = room.getDoorByButton(buttonPressed)
            # find item associated with that button
            chosenItem = room.getItemByButton(buttonPressed)

            if chosenDoor == None and chosenItem == None:
                print("This is not a valid input!")
                print("Please try again")
                self.askWhatToDo(room)
            else:
                if chosenDoor != None:
                    self.openDoor(chosenDoor, room)
                    #if chosen door is Penny's -> "Knock! Knock! Knock! Penny"
                elif chosenItem != None:
                    self.takeItem(chosenItem, room)
                    self.askWhatToDo(room)
    
    def takeItem(self, item, room):
        room.removeItem(item)
        self.backpack.items.append(item)
        print("Added " + item.name + " to backpack.")


    def end(self):
        print("Thank you for playing!")
        print("Would you like to play again (y/n)?")
        playAgain = input(">>> ")

        if playAgain.lower() == "y" or playAgain.lower() == "yes":
            self.start()
        elif playAgain.lower() == "n" or playAgain.lower() == "no":
            sys.exit()
        else:
            self.end()
