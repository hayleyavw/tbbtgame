from rooms.room import Room
from objects.door import Door
from objects.item import Item

class LLab(Room):

    def __init__(self):

        self.name = "Leonard's Lab"
        self.description = "It appears as though you are in Leonard's lab.\nThere are a number of experiments set up, and you notice a large red button labelled \"DO NOT PRESS\""

        self.doors = [
        	Door("Leave the lab", "Leave", "Hall"),
        	Door("Click the large red button labelled \"DO NOT PRESS\"", "Portal", "Portal")
        ]

        self.items = [
		]

		#laser that opens portal
