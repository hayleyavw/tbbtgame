from rooms.room import Room
from objects.door import Door

class SRoom(Room):
    
    def __init__(self):

        self.name = "Sheldon's Room"
        self.description = "You've reached Sheldon's room"

        self.doors = [
            Door("Enter the hallway", "Hallway", "Hallway"), 
        ]

        self.items = []
        #bottle city of candor
