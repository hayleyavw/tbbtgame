from rooms.room import Room
from objects.door import Door
from objects.item import Item

class Lobby(Room):

    def __init__(self):

        self.name = "Lobby"
        self.description = "You're in the lobby of the apartment building."

        self.doors = [
            Door("Go up the stairs", "Stairwell", "Stairwell"),
            Door("Take the elevator", "Elevator", "Elevator"),
            Door("Go into the laundry", "Laundry Room", "Laundry Room"),
            Door("Check the mail", "Mail", "Letterbox")
        ]

        self.items = [
		]
        #check the mail