from rooms.room import Room
from objects.door import Door
from objects.item import Item

class LaundryRoom(Room):

    def __init__(self):

        self.name = "Laundry Room"
        self.description = "You're in the laundry room."

        self.doors = [
            Door("Enter the lobby", "Lobby", "Lobby")
        ]

        self.items = [
		]

        #add laundry shute