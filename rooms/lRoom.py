from rooms.room import Room
from objects.door import Door

class LRoom(Room):
    
    def __init__(self):

        self.name = "Leonard's Room"
        self.description = "You've reached Leonard's room"

        self.doors = [
            Door("Enter the hallway", "Hallway", "Hallway"), 
        ]

        self.items = []
        #bottle city of candor
