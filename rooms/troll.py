from rooms.room import Room
from objects.door import Door
from objects.item import Item

class Troll(Room):
    
    def __init__(self):

        self.name = "Troll"
        self.description = "With what?"

        self.doors = [
        	Door("Hit troll with axe", "Troll", "Troll"),
        	Door("Give up", "Give up", "Lounge")
        ]
        
        self.items = [
        ]
