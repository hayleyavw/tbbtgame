from rooms.room import Room
from objects.door import Door

class PBathroom(Room):
    
    def __init__(self):

        self.name = "Penny's Bathroom"
        self.description = "You're in Penny's bathroom"

        self.doors = [
        	Door("Go into Penny's room", "Door", "Penny's Room"),
        	Door("Go down the laundry shute", "Laundry Shute", "Laundry Room")
        ]
        
        self.items = [] # adhesive ducks
