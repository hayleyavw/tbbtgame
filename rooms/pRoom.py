from rooms.room import Room
from objects.door import Door
from objects.item import Item

class PRoom(Room):

    def __init__(self):

        self.name = "Penny's Room"
        self.description = "You're in Penny's room"

        self.doors = [
            Door("Go into the bathroom", "Bathroom", "Penny's Bathroom"),
            Door("Go into the lounge", "Lounge", "Penny's Lounge")
        ]

        self.items = [
        ]
