from rooms.room import Room
from objects.door import Door
from objects.item import Item

class Roof(Room):

    def __init__(self):

        self.name = "Roof"
        self.description = "You're on the roof of the apartment building.\nThe door slams shut behind you.\nYou notice a blue key on the ground."

        self.doors = [
            Door("Go back downstairs", "Downstairs", "Corridor", "Red Key")
        ]

        self.items = [
        	Item("Pick up the key", "Blue Key")
        ]
