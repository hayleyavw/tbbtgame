from rooms.room import Room
from objects.door import Door

class Hall(Room):

    def __init__(self):

        self.name = "Hall"
        self.description = "You find yoursel in a hallway."

        self.doors = [
        	Door("Check out Leonard's lab", "Leonard's Lab", "Leonard's Lab", "Green Key"),
        	Door("Enter Sheldon's office", "Sheldon's office", "Sheldon's office")
        ]

        self.items = []
