from rooms.room import Room
from objects.door import Door
from objects.item import Item

class SOffice(Room):

    def __init__(self):

        self.name = "Sheldon's Office"
        self.description = "You are in Sheldon's office"

        self.doors = [
        	Door("Leave the office", "Door", "")
        ]

        self.items = [
		]
