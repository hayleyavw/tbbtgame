from rooms.room import Room
from objects.door import Door
from objects.item import Item

class Elevator(Room):

    def __init__(self):

        self.name = "Elevator"
        self.description = "The elevator doesn't work!"

        self.doors = [
            Door("Exit the elevator", "Corridor", "Corridor"),
            Door("Go through the mysterious portal", "Portal", "Cafeteria")
        ]

        self.items = [
		]
