from rooms.room import Room
from objects.door import Door
from objects.item import Item

class Portal(Room):

    def __init__(self):

        self.name = "Portal"
        self.description = "The large red button opened a portal. Go through it?"

        self.doors = [
        	Door("Leave the lab and pretend you were never there", "Leave", "Hall"),
        	Door("Go through the portal", "Portal", "Cheesecake Factory")
        ]

        self.items = [
		]

