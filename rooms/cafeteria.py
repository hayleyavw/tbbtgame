from rooms.room import Room
from objects.door import Door

class Cafeteria(Room):

    def __init__(self):

        self.name = "Cafeteria"
        self.description = "You have reached the university's cafeteria."

        self.doors = [
        	Door("Leave cafeteria", "Corridor", "Hall")
        ]

        self.items = []
