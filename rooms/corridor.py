from rooms.room import Room
from objects.door import Door
from objects.item import Item

class Corridor(Room):

    def __init__(self):

        self.name = "Corridor"
        self.description = "You're in the corridor between apartments."

        self.doors = [
            Door("Enter Leonard and Sheldon's apartment", "Apartment 4A", "Lounge", "Blue Key"),
            Door("Enter Penny's apartment", "Apartment 4B", "Penny's Lounge"),
            Door("Go down the stairs", "Stairwell", "Stairwell"),
            Door("Go up to the roof", "Roof", "Roof"),
            Door("Take the elevator", "Elevator", "Elevator")
        ]

        self.items = [
		]
