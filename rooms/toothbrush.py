from rooms.room import Room
from objects.door import Door
from objects.item import Item

class Toothbrush(Room):
    
    def __init__(self):

        self.name = "Toothbrush"
        self.description = "Just to annoy sheldon, you decide to use his toothbrush. When you pick it up, you find a green key hiding underneath."

        self.doors = [
        ]
        
        self.items = [
        	Item("Pick up the Green Key", "Key")
        ]
        #Sheldon's toothbrush
