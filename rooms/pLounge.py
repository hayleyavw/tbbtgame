from rooms.room import Room
from objects.door import Door
from objects.item import Item

class PLounge(Room):

    def __init__(self):

        self.name = "Penny's Lounge"
        self.description = "You're in Penny's lounge"

        self.doors = [
            Door("Enter Penny's room", "Penny's Room", "Penny's Room"),
            Door("Walk into the kitchen", "Kitchen", "Kitchen"),
            Door("Leave the apartment", "Corridor", "Corridor")
        ]

        self.items = []
