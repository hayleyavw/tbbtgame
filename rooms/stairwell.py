from rooms.room import Room
from objects.door import Door

class Stairwell(Room):

    def __init__(self):

        self.name = "Stairwell"
        self.description = "You are in the stairwell"

        self.doors = [
        	Door("Go upstairs", "Upstairs", "Corridor"),
        	Door("Go downstairs", "Downstairs", "Lobby")
        ]

        self.items = []
