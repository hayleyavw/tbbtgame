from rooms.room import Room
from objects.door import Door

class Bathroom(Room):
    
    def __init__(self):

        self.name = "Bathroom"
        self.description = "You're in the bathroom"

        self.doors = [
            Door("Go back", "Go back into the hallway", "Hallway"),
            Door("Go down the laundry shute", "Laundry Shute", "Laundry Room"),
            Door("Use Sheldon's toothbrush", "Toothbrush", "Toothbrush")
        ]

        self.items = [
        ]
        #Sheldon's toothbrush
