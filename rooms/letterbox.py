from rooms.room import Room
from objects.door import Door
from objects.item import Item

class Letterbox(Room):

    def __init__(self):

        self.name = "Letterbox"
        self.description = "No snail mail, but there is a red key in the letterbox."

        self.doors = [
            Door("Close the letterbox", "Leave letterbox", "Lobby")
        ]

        self.items = [
            Item("Pick up the key", "Red Key")
		]
        #check the mail