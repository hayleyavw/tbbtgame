from rooms.room import Room
from objects.door import Door

class Hallway(Room):

    def __init__(self):

        self.name = "Hallway"
        self.description = "You're in the hallway."

        self.doors = [
            Door("Go to the lounge", "Lounge", "Lounge"),
            Door("Go into the bathroom", "Bathroom", "Bathroom"),
            Door("Enter Leonard's Room", "Leonard's Room", "Leonard's Room"),
            Door("Enter Sheldon's Room", "Sheldon's Room", "Sheldon's Room")
        ]

        self.items = []
