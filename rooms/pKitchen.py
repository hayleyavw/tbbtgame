from rooms.room import Room
from objects.door import Door
from objects.item import Item

class PKitchen(Room):

    def __init__(self):

        self.name = "Penny's Kitchen"
        self.description = "You're in Penny's kitchen"

        self.doors = [
            Door("Walk into the lounge", "Lounge", "Penny's Lounge")
        ]

        self.items = []
        #cereals sorted by fibre content
