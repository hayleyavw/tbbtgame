from rooms.room import Room
from objects.door import Door
from objects.item import Item

class TextGame(Room):
    
    def __init__(self):

        self.name = "textgame"
        self.description = "A troll stands in your way"

        self.doors = [
        	Door("Hit Troll", "Troll", "Troll"),
        ]
        
        self.items = [
        ]
