from rooms.room import Room
from objects.door import Door
from objects.item import Item
from objects.passphrase import Passphrase

class Lounge(Room):

	def __init__(self):
		self.passphrase = Passphrase()
		self.name = "Lounge"
		self.description = "You're in Leonard and Sheldons's lounge"

		self.doors = [
	        Door("Enter the hallway", "Hallway", "Hallway"),
	        Door("Walk into the kitchen", "Kitchen", "Kitchen"),
	        Door("Leave the apartment", "Corridor", "Corridor"),
	        Door("Play on Sheldon's laptop", "Laptop", "Laptop")
		]

		self.items = []
