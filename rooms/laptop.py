from rooms.room import Room
from objects.door import Door
from objects.item import Item

class Laptop(Room):
    
    def __init__(self):

        self.name = "Laptop"
        self.description = "Sheldon appears to have left his computer while in the middle of a text-based game, would you like to pla?"

        self.doors = [
        	Door("Yes", "Yes", "textgame"),
        	Door("No", "No", "Lounge")
        ]
        
        self.items = [
        ]
