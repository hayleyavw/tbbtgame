class Passphrase(object):

	def __init__(self):
		self.access = False;
	
	def displayPassphrase(self):
		print("\nThe secret passphrase is: \"Can't we just go to the Big Boy?\"")
		print("      _..----.._     ")
		print("    .'     o    '.   ")   
		print("   /   o       o  \  ")
		print("  |o        o     o| ")
		print("  /'-.._o     __.-'\ ")
		print("  \      `````     / ")
		print("  |``--........--'`| ")
		print("   \              /  ")
		print("    `'----------'`\n\n")

	def updateStateToTrue(self):
		self.access = True